/*
 * Copyright (C) 2013-2016 Canonical Ltd
 *
 * This file is part of Ubuntu Calendar App
 *
 * Ubuntu Calendar App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Qt.labs.settings 1.0
import Ubuntu.Components.Pickers 1.3
import "CustomPickers"

Page {
    id: settingsPage
    objectName: "settings"

    property EventListModel eventModel
    property Settings settings: undefined

    Binding {
        target: settingsPage.settings
        property: "showWeekNumber"
        value: weekCheckBox.checked
        when: settings
    }

    Binding {
        target: settingsPage.settings
        property: "showLunarCalendar"
        value: lunarCalCheckBox.checked
        when: settings
    }

    visible: false

    header: PageHeader {
        title: i18n.tr("Settings")
        leadingActionBar.actions: Action {
            text: i18n.tr("Back")
            iconName: "back"
            onTriggered: pop()
        }
    }

    RemindersModel {
        id: remindersModel
    }

    Flickable {
        id: settingsFlickable
        clip: true
        flickableDirection: Flickable.AutoFlickIfNeeded

        anchors {
            topMargin: settingsPage.header.height + units.gu(1)
            fill: parent
        }

        contentHeight: settingsColumn.childrenRect.height

        Column {
            id: settingsColumn
            objectName: "settingsColumn"

            spacing: units.gu(0.5)
            anchors.fill: parent

            ListItem {
                height: weekNumberLayout.height + divider.height
                divider { visible: false; }
                ListItemLayout {
                    id: weekNumberLayout
                    title.text: i18n.tr("Show week numbers")
                    CheckBox {
                        id: weekCheckBox
                        objectName: "weekCheckBox"
                        SlotsLayout.position: SlotsLayout.Last
                        checked: settings ? settings.showWeekNumber : false
                    }
                }

                onClicked: {
                    weekCheckBox.checked = !weekCheckBox.checked;
                }
            }

            ListItem {
                height: lunarCalLayout.height + divider.height
                divider { visible: false; }
                ListItemLayout {
                    id: lunarCalLayout
                    title.text: i18n.tr("Display Chinese calendar")
                    CheckBox {
                        id: lunarCalCheckBox
                        objectName: "lunarCalCheckbox"
                        SlotsLayout.position: SlotsLayout.Last
                        checked: settings ? settings.showLunarCalendar : false
                    }
                }

                onClicked: {
                    lunarCalCheckBox.checked = !lunarCalCheckBox.checked;
                }
            }

            ListItem {
                height: businessHoursLayout.height + divider.height
                divider { visible: false; }
                width: parent.width

                ListItemLayout {
                    id: businessHoursLayout
                    title.text: i18n.tr("Business hours")

                    height: businessStartInput.height

                    Row {
                        id: businessHoursSettingRow
                        property date startDateToBeSet: {
                            var dt = new Date()
                            dt.setHours(settings.businessHourStart,0,0,0)
                            return dt
                        }
                        property date endDateToBeSet: {
                          var dt = new Date()
                          dt.setHours(settings.businessHourEnd,0,0,0)
                          return dt
                        }

                        onEndDateToBeSetChanged: setBusinessHours()
                        onStartDateToBeSetChanged: setBusinessHours()

                        function setBusinessHours() {
                            if (startDateToBeSet < endDateToBeSet) {
                                settings.businessHourStart = startDateToBeSet.getHours()
                                settings.businessHourEnd = endDateToBeSet.getHours()
                                businessStartInput.text = Qt.formatTime(startDateToBeSet, Qt.SystemLocaleShortDate);
                                businessEndInput.text = Qt.formatTime(endDateToBeSet, Qt.SystemLocaleShortDate);
                            }
                        }

                        function openDatePicker (element, caller, callerProperty, mode) {
                            element.highlighted = true;
                            var picker = NewPickerPanel.openDatePicker(caller, callerProperty, mode);
                            if (!picker) return;
                            picker.closed.connect(function () {
                                element.highlighted = false;
                            });
                        }

                        NewEventEntryField {
                            id: businessStartInput

                            objectName: "businessStartInput"
                            text: "" //Qt.formatTime(businessHoursSettingRow.startDateToBeSet, Qt.SystemLocaleShortDate);
                            visible: true

                            MouseArea{
                                anchors.fill: parent
                                onClicked: businessHoursSettingRow.openDatePicker(businessStartInput, businessHoursSettingRow, "startDateToBeSet", "Hours")
                            }
                        }

                        Label {
                            text: " - "
                            height: parent.height
                            verticalAlignment: Text.AlignVCenter
                        }

                        NewEventEntryField {
                            id: businessEndInput

                            objectName: "businessEndInput"
                            text: "" //Qt.formatTime(businessHoursSettingRow.endDateToBeSet, Qt.SystemLocaleShortDate);
                            visible: true

                            MouseArea{
                                anchors.fill: parent
                                onClicked: businessHoursSettingRow.openDatePicker(businessEndInput, businessHoursSettingRow, "endDateToBeSet", "Hours")
                            }
                        }

                        SlotsLayout.position: SlotsLayout.Last
                    }
                }
            }

            ListItem {
                id: refreshIntervalItem

                visible: themeModel && refreshIntervalModel.count > 0
                height: visible ? refreshIntervalLayout.height + divider.height : 0
                divider { visible: false; }

                Component.onCompleted: {
                    if (!refreshIntervalOptionSelector.model) {
                        return
                    }

                    for (var i = 0; i < refreshIntervalOptionSelector.model.count; ++i) {
                        var refreshInterval = refreshIntervalOptionSelector.model.get(i)
                        if (refreshInterval.interval === settings.refreshInterval) {
                            refreshIntervalOptionSelector.selectedIndex = i
                            return
                        }
                    }

                    refreshIntervalOptionSelector.selectedIndex = 0
                }

                SlotsLayout {
                    id: refreshIntervalLayout

                    mainSlot: Item {
                        height: refreshIntervalOptionSelector.height

                        OptionSelector {
                            id: refreshIntervalOptionSelector

                            text: i18n.tr("Data refresh interval")
                            model: refreshIntervalModel
                            containerHeight: itemHeight * 4

                            delegate: OptionSelectorDelegate {
                                text: model.text
                                height: units.gu(4)
                            }

                            onDelegateClicked: {
                                settings.refreshInterval = model.get(index).interval
                                refreshDataTimer.start()
                            }
                        }
                    }
                }
            }

            Label {
                id: backgroundSuspensionNoteLabel
                width: parent.width
                anchors {
                    left: parent.left
                    leftMargin: units.gu(2)
                    right: parent.right
                    rightMargin: units.gu(2)
                }
                font.italic: true
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignJustify
                text: i18n.tr("Note: Automatic syncronisation currently only works while the app is open in the foreground. Alternatively set background suspension to off. Then sync will work while the app is open in the background.")
            }

            ListModel {
                id: refreshIntervalModel
                Component.onCompleted: initialize()

                function initialize() {
                    refreshIntervalModel.append({"interval": 5, "text": i18n.tr("%1 minute", "%1 minutes", 5).arg(5)})
                    refreshIntervalModel.append({"interval": 10, "text": i18n.tr("%1 minute", "%1 minutes", 10).arg(10)})
                    refreshIntervalModel.append({"interval": 15, "text": i18n.tr("%1 minute", "%1 minutes", 15).arg(15)})
                    refreshIntervalModel.append({"interval": 15, "text": i18n.tr("%1 minute", "%1 minutes", 30).arg(30)})
                    refreshIntervalModel.append({"interval": 60, "text": i18n.tr("%1 hour", "%1 hours", 1).arg(1)})
                    refreshIntervalModel.append({"interval": 180, "text": i18n.tr("%1 hour", "%1 hours", 3).arg(3)})
                    refreshIntervalModel.append({"interval": 320, "text": i18n.tr("%1 hour", "%1 hours", 6).arg(6)})
                    refreshIntervalModel.append({"interval": 640, "text": i18n.tr("%1 hour", "%1 hours", 12).arg(12)})
                }
            }

            ListItem {
                id: defaultReminderItem

                visible: defaultReminderOptionSelector.model && defaultReminderOptionSelector.model.count > 0
                height: visible ? defaultReminderLayout.height + divider.height : 0
                divider { visible: false; }

                Connections {
                    target: remindersModel
                    onLoaded: {
                        if (!defaultReminderOptionSelector.model) {
                            return
                        }

                        for (var i = 0; i < defaultReminderOptionSelector.model.count; ++i) {
                            var reminder = defaultReminderOptionSelector.model.get(i)
                            if (reminder.value === settings.reminderDefaultValue) {
                                defaultReminderOptionSelector.selectedIndex = i
                                return
                            }
                        }

                        defaultReminderOptionSelector.selectedIndex = 0
                    }
                }

                SlotsLayout {
                    id: defaultReminderLayout

                    mainSlot: Item {
                        height: defaultReminderOptionSelector.height

                        OptionSelector {
                            id: defaultReminderOptionSelector

                            text: i18n.tr("Default reminder")
                            model: remindersModel
                            containerHeight: itemHeight * 4

                            delegate: OptionSelectorDelegate {
                                text: label
                                height: units.gu(4)
                            }

                           onDelegateClicked: settings.reminderDefaultValue = model.get(index).value
                        }
                    }
                }
            }

            ListItem {
                id: newEventDefaultLengthItem

                visible: eventLengthModel && eventLengthModel.count > 0
                height: visible ? newEventDefaultLengthLayout.height + divider.height : 0
                divider { visible: false; }

                Component.onCompleted: {
                    if (!newEventDefaultLengthOptionSelector.model) {
                        return
                    }

                    for (var i = 0; i < newEventDefaultLengthOptionSelector.model.count; ++i) {
                        var length = newEventDefaultLengthOptionSelector.model.get(i)
                        if (length.value === settings.newEventDefaultLength) {
                            newEventDefaultLengthOptionSelector.selectedIndex = i
                            return
                        }
                    }

                    newEventDefaultLengthOptionSelector.selectedIndex = 0
                }

                SlotsLayout {
                    id: newEventDefaultLengthLayout

                    mainSlot: Item {
                        height: newEventDefaultLengthOptionSelector.height

                        OptionSelector {
                            id: newEventDefaultLengthOptionSelector

                            text: i18n.tr("Default length of new event")
                            model: eventLengthModel
                            containerHeight: itemHeight * 4

                            delegate: OptionSelectorDelegate {
                                text: label
                                height: units.gu(4)
                            }

                            onDelegateClicked: {
                                settings.newEventDefaultLength = model.get(index).value
                            }
                        }
                    }
                }
            }

            ListModel {
                id: eventLengthModel
                Component.onCompleted: initialize()

                function initialize() {
                    eventLengthModel.append({"label": i18n.tr("%1 minutes").arg(5), "value": 5})
                    eventLengthModel.append({"label": i18n.tr("%1 minutes").arg(10), "value": 10})
                    eventLengthModel.append({"label": i18n.tr("%1 minutes").arg(15), "value": 15})
                    eventLengthModel.append({"label": i18n.tr("%1 minutes").arg(30), "value": 30})
                    eventLengthModel.append({"label": i18n.tr("%1 minutes").arg(45), "value": 45})
                    eventLengthModel.append({"label": i18n.tr("%1 minutes").arg(60), "value": 60})
                    eventLengthModel.append({"label": i18n.tr("%1 minutes").arg(90), "value": 90})
                    eventLengthModel.append({"label": i18n.tr("%1 minutes").arg(120), "value": 120})
                }
            }

            ListItem {
                visible: defaultCalendarOptionSelector.model && defaultCalendarOptionSelector.model.length > 0
                height: visible ? defaultCalendarLayout.height + divider.height : 0
                divider { visible: false; }

                Component.onCompleted: {
                    if (!eventModel || !defaultCalendarOptionSelector.model) {
                        return
                    }

                    var defaultCollectionId = eventModel.getDefaultCollection().collectionId
                    for (var i = 0; i < defaultCalendarOptionSelector.model.length; ++i) {
                        if (defaultCalendarOptionSelector.model[i].collectionId === defaultCollectionId) {
                            defaultCalendarOptionSelector.selectedIndex = i
                            return
                        }
                    }

                    defaultCalendarOptionSelector.selectedIndex = 0
                }

                SlotsLayout {
                    id: defaultCalendarLayout

                    mainSlot: Item {
                        height: defaultCalendarOptionSelector.height

                        OptionSelector {
                            id: defaultCalendarOptionSelector

                            text: i18n.tr("Default calendar")
                            model: settingsPage.eventModel ? settingsPage.eventModel.getWritableAndSelectedCollections() : []
                            containerHeight: (model && (model.length > 1) ? itemHeight * model.length : itemHeight)

                            Connections {
                                target: settingsPage.eventModel ? settingsPage.eventModel : null
                                onModelChanged: {
                                    defaultCalendarOptionSelector.model = settingsPage.eventModel.getWritableAndSelectedCollections()
                                }
                                onCollectionsChanged: {
                                    defaultCalendarOptionSelector.model = settingsPage.eventModel.getWritableAndSelectedCollections()
                                }
                            }

                            delegate: OptionSelectorDelegate {
                                text: modelData.name
                                height: units.gu(4)

                                UbuntuShape{
                                    anchors {
                                        right: parent.right
                                        rightMargin: units.gu(4)
                                        verticalCenter: parent.verticalCenter
                                    }

                                     width: height
                                     height: parent.height - units.gu(2)
                                     color: modelData.color
                                }
                            }

                            onDelegateClicked: settingsPage.eventModel.setDefaultCollection(model[index].collectionId)
                        }
                    }
                }
            }

            ListItem {
                id: themeItem

                visible: themeModel && themeModel.count > 0
                height: visible ? themeLayout.height + divider.height : 0
                divider { visible: false; }

                Component.onCompleted: {
                    if (!themeOptionSelector.model) {
                        return
                    }

                    for (var i = 0; i < themeOptionSelector.model.count; ++i) {
                        var theme = themeOptionSelector.model.get(i)
                        if (theme.value === settings.selectedTheme) {
                            themeOptionSelector.selectedIndex = i
                            return
                        }
                    }

                    themeOptionSelector.selectedIndex = 0
                }

                SlotsLayout {
                    id: themeLayout

                    mainSlot: Item {
                        height: themeOptionSelector.height

                        OptionSelector {
                            id: themeOptionSelector

                            text: i18n.tr("Theme")
                            model: themeModel
                            containerHeight: itemHeight * 4

                            delegate: OptionSelectorDelegate {
                                text: label
                                height: units.gu(4)
                            }

                            onDelegateClicked: {
                                settings.selectedTheme = model.get(index).value
                                mainView.setCurrentTheme();
                            }
                        }
                    }
                }
            }

            ListModel {
                id: themeModel
                Component.onCompleted: initialize()

                function initialize() {
                    themeModel.append({"label": i18n.tr("System theme"), "value": "System"})
                    themeModel.append({"label": i18n.tr("SuruDark theme"), "value": "SuruDark"})
                    themeModel.append({"label": i18n.tr("Ambiance theme"), "value": "Ambiance"})
                }
            }
        }
    }
}
